// _omdlg32.cpp : _omdlg32のエントリポイント
//

#include <windows.h>

FARPROC p_ChooseColorA;

//FARPROC p_GetOpenFileNameA;
typedef BOOL (WINAPI *D_GetOpenFileNameA)(LPOPENFILENAMEA);
D_GetOpenFileNameA p_GetOpenFileNameA;

FARPROC p_GetSaveFileNameA;
FARPROC p_PageSetupDlgA;
FARPROC p_PrintDlgA;

HINSTANCE h_original;

BOOL APIENTRY DllMain( HANDLE hModule, 
                    DWORD  ul_reason_for_call, 
                    LPVOID lpReserved
                  )
{
 switch( ul_reason_for_call )
 {
 case DLL_PROCESS_ATTACH:
     h_original = LoadLibraryA( "comdlg32.dll" );
     if ( h_original == NULL )
         return FALSE;
       p_ChooseColorA = GetProcAddress( h_original, "ChooseColorA" );
       p_GetOpenFileNameA = (D_GetOpenFileNameA)GetProcAddress( h_original, "GetOpenFileNameA" );
       p_GetSaveFileNameA = GetProcAddress( h_original, "GetSaveFileNameA" );
       p_PageSetupDlgA = GetProcAddress( h_original, "PageSetupDlgA" );
       p_PrintDlgA = GetProcAddress( h_original, "PrintDlgA" );
     break;
 case DLL_THREAD_ATTACH:
     break;
 case DLL_THREAD_DETACH:
     break;
 case DLL_PROCESS_DETACH:
     FreeLibrary( h_original );
     break;
 default:
     break;
 }
 return TRUE;
 }

__declspec( naked ) void d_ChooseColorA() { _asm{ jmp p_ChooseColorA } }

#include <atlbase.h>
#include <atlconv.h>

UINT asizeFilter(LPCSTR lpstr)
{
  if(lpstr[0] == NULL && lpstr[1]==NULL)return 2;
  const char *s = lpstr;
  while(1)
  {
    UINT l = lstrlenA(s);
    s+=l+1;
    if(l==0 && s[0]==NULL)return s-lpstr+1;
  }
}
void CopyA2W_Filter(LPWSTR dst, LPCSTR src)
{
  if(src[0]==NULL && src[1]==NULL)
  {
    dst[0] = NULL;
    dst[1] = NULL;
    return;
  }
  const char *s = src;
  while(1)
  {
    lstrcatW(dst, CA2W(s));
    UINT l = lstrlenA(s);
    s += l+1;
    dst += lstrlenW(dst)+1;
    if(l==0 && s[0] == NULL)
    {
      dst[0]=NULL;
      return;
    }
  }
}

//__declspec( naked ) void d_GetOpenFileNameA() { _asm{ jmp p_GetOpenFileNameA } }
BOOL WINAPI d_GetOpenFileNameA(LPOPENFILENAMEA ofna)
{
  wchar_t *wcFilter = NULL;
  if(ofna->lpstrFilter!=NULL)
  {
    wcFilter = (wchar_t *)calloc(asizeFilter(ofna->lpstrFilter)+2, sizeof(wchar_t));
    CopyA2W_Filter(wcFilter, ofna->lpstrFilter);
  }
  
  
  CA2W wcInitialDir(ofna->lpstrInitialDir);
  CA2W wcTitle(ofna->lpstrTitle);
  CA2W wcDefExt(ofna->lpstrDefExt);
  CA2W wcTemplateName(ofna->lpTemplateName);
  //CA2W wcPrompt(ofna->lpstrPrompt);
  
  /*
  OutputDebugStringA("d_GetOpenFileNameA: ");
  OutputDebugStringA(ofna->lpstrFilter);
  OutputDebugStringA(ofna->lpstrCustomFilter);
  */
  
  wchar_t* wcCustomFilter = NULL;
  if(ofna->lpstrCustomFilter!=NULL)
  {
    wcCustomFilter = (wchar_t *)malloc(sizeof(wchar_t)*5001);
    lstrcpynW(wcCustomFilter, CA2W(ofna->lpstrCustomFilter), 5000);
  }
  wchar_t* wcFile = NULL;
  if(ofna->lpstrFile!=NULL)
  {
    wcFile         = (wchar_t *)malloc(sizeof(wchar_t)*5001);
    lstrcpynW(wcFile,         CA2W(ofna->lpstrFile), 5000);
  }
  wchar_t* wcFileTitle = NULL;
  if(ofna->lpstrFileTitle!=NULL)
  {
    wcFileTitle    = (wchar_t *)malloc(sizeof(wchar_t)*5001);
    lstrcpynW(wcFileTitle,    CA2W(ofna->lpstrFileTitle), 5000);
  }
  
  //OutputDebugStringW(wcCustomFilter);
  OPENFILENAMEW ofnw = {0};
  
  ofnw.lStructSize = sizeof(OPENFILENAMEW);
  ofnw.hwndOwner   = ofna->hwndOwner;
  ofnw.hInstance   = ofna->hInstance;
  ofnw.lpstrFilter = wcFilter;
  ofnw.lpstrCustomFilter = wcCustomFilter;
  ofnw.nMaxCustFilter = 5000;
  ofnw.nFilterIndex = ofna->nFilterIndex;
  ofnw.lpstrFile   = wcFile;
  ofnw.nMaxFile    = 5000;
  ofnw.lpstrFileTitle = wcFileTitle;
  ofnw.nMaxFileTitle = 5000;
  ofnw.lpstrInitialDir = wcInitialDir;
  ofnw.lpstrTitle  = wcTitle;
  ofnw.Flags       = ofna->Flags;
  ofnw.nFileOffset = ofna->nFileOffset;
  ofnw.nFileExtension = ofna->nFileExtension;
  ofnw.lpstrDefExt = wcDefExt;
  ofnw.lCustData  = ofna->lCustData;
  ofnw.lpfnHook   = ofna->lpfnHook;
  ofnw.lpTemplateName = wcTemplateName;
  //ofnw.lpEditInfo = ofna->lpEditInfo;
  //ofnw.lpstrPrompt = wcPrompt;
  ofnw.pvReserved = ofna->pvReserved;
  ofnw.dwReserved = ofna->dwReserved;
  ofnw.FlagsEx    = ofna->FlagsEx;
  
  BOOL ret = GetOpenFileNameW(&ofnw);
  
  if(wcFilter!=NULL)
  {
    free(wcFilter);
    wcFilter=NULL;
    ofnw.lpstrFilter=NULL;
  }
  
  if(ret==FALSE)goto end_GetOpenFileNameA;
  
  
  ofna->lStructSize = ofnw.lStructSize;
  //ofna->hwndOwner   = ofnw.hwndOwner;
  //ofna->hInstance   = ofnw.hInstance;
  //ofna->lpstrFilter = strFilter;
  if(ofnw.lpstrCustomFilter!=NULL)lstrcpynA(ofna->lpstrCustomFilter, CW2A(ofnw.lpstrCustomFilter), ofna->nMaxCustFilter+1);
  //ofna->nMaxCustFilter = ofnw.nMaxCustFilter;
  //ofna->nFilterIndex = ofnw.nFilterIndex;
  {
  
    DWORD dwShortPath = GetShortPathNameW(ofnw.lpstrFile, NULL, 0);
    wchar_t *shortpath = (wchar_t *)malloc(sizeof(wchar_t)*(dwShortPath+2));
    GetShortPathNameW(ofnw.lpstrFile, shortpath, dwShortPath+1);
    CW2A strFile(shortpath);
    free(shortpath);
    //OutputDebugStringA(strFile);
    if(lstrlenA(strFile) > ofna->nMaxFile)
    {
      ret = FALSE;
      goto end_GetOpenFileNameA;
    }
    lstrcpynA(ofna->lpstrFile, strFile, ofna->nMaxFile+1);
  }
  //ofna->nMaxFile    = ofnw.nMaxFile;
  if(ofnw.lpstrFileTitle!=NULL)lstrcpynA(ofna->lpstrFileTitle, CW2A(ofnw.lpstrFileTitle), ofna->nMaxFileTitle+1);
  //ofna->nMaxFileTitle = ofnw.nMaxFileTitle;
  //ofna->lpstrInitialDir = strInitialDir;
  //ofna->lpstrTitle  = strTitle;
  //ofna->Flags       = ofnw.Flags;
  ofna->nFileOffset = (ofna->lpstrFile!=NULL) ? PathFindFileNameA(ofna->lpstrFile) - ofna->lpstrFile : 0;
  ofna->nFileExtension = (ofna->lpstrFile!=NULL) ? PathFindExtensionA(ofna->lpstrFile) - ofna->lpstrFile : 0;
  //ofna->lpstrDefExt = strDefExt;
  //ofna->lCustData  = ofnw.lCustData;
  ofna->lpfnHook   = ofnw.lpfnHook;
  //ofna->lpTemplateName = strTemplateName;
  //ofna->lpEditInfo = ofnw.lpEditInfo;
  //ofna->lpstrPrompt = strPrompt;
  ofna->pvReserved = ofnw.pvReserved;
  ofna->dwReserved = ofnw.dwReserved;
  //ofna->FlagsEx    = ofnw.FlagsEx;
  
end_GetOpenFileNameA:
  if(wcCustomFilter!=NULL)free(wcCustomFilter);
  if(wcFile!=NULL)free(wcFile);
  if(wcFileTitle!=NULL)free(wcFileTitle);
  return ret;
}

__declspec( naked ) void d_GetSaveFileNameA() { _asm{ jmp p_GetSaveFileNameA } }
__declspec( naked ) void d_PageSetupDlgA() { _asm{ jmp p_PageSetupDlgA } }
__declspec( naked ) void d_PrintDlgA() { _asm{ jmp p_PrintDlgA } }
